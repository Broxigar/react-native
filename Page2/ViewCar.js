import React, {Component} from 'react';
import {Text, View, ScrollView, StyleSheet, FlatList, Image} from 'react-native';
import {List, ListItem} from 'react-native-elements';

export default class ViewRent extends Component {
     constructor(props){
         super(props)
         this.state = {
             posts: [],
             userID:3,
             loading: false,
             refreshing: false
         }
     }

     componentDidMount(){
         this.getData()
     }

     getData(){
         //this.setState({ loading: true});
         fetch('http://pte.novasoftware.com.au:3090/api/carList')
         .then(response=>response.json())
         .then(data => {
            console.log(data)
            this.setState({
                posts:data,
                //loading: false,
                //refreshing: false
            })
         }         
        )
     }

    //  MapData() {
    //     return this.state.posts.map(function(post, i){
    //       return(
    //         <View key={i}>
    //           <Text>{post.userName}</Text>
    //           <Text>{post.content}</Text>
    //         </View>
    //       );
    //     });
    //   }

    renderSeparator=() => {
        return(
            <View
                style={{
                    height:1,
                    width:'86%',
                    backgroundColor: '#8DB255',
                    marginLeft:'14%',
                }}
            />
        )
    }
     render(){
        if((this.state.posts.length) == 0){
            return <View>
                <Text>
                    abcdfefrggtjjn
                    </Text>
            </View>
            
           }

        
        return(
             
        // <View>
        //         {this.MapData()}
        //     </View>
        
        <List containerStyle={{borderTopWidth:0, borderBottomWidth:0}}>
            {console.log(this.state.posts)}
            <FlatList
                data={this.state.posts}
                renderItem={({ item }) => (
                    <ListItem
                        roundAvatar
                        //subtitle={item.userName}
                        title={item.content}
                        subtitle={item.dateTime}
                        onPress={()=>this.props.navigation.navigate('viewPost',{idPOST: item.idPOST})}
                        //avatar={item.urlImages}
                        containerStyle={{borderBottomWidth: 0 }}                       
                    />
                )}

                //keyExtractor={post => post.userName}
                ItemSeparatorComponent={this.renderSeparator}
            />
        </List>
         )
     }
    }
